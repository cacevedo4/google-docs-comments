const documentContent = `
  <h1>Titulo del documento</h1>
  <br />
  <br />
  <h3>Introducción</h3>
  <p>
    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Eveniet blanditiis vel tenetur. Iste, nam est aspernatur repellendus earum inventore officiis nisi eligendi commodi iusto adipisci nobis a. Quam, suscipit sunt!
    Illum vitae doloremque, quam maiores quisquam corrupti eum, saepe sunt sit minima corporis nesciunt esse odit iure odio et deserunt voluptas! Delectus quae error distinctio asperiores neque nemo quo maiores.
    Voluptatibus laudantium, deleniti quasi officia harum quae neque temporibus libero aperiam quidem ducimus ipsum quis similique totam repellat exercitationem fugiat? Maxime, fugiat eligendi! Delectus voluptates minima facilis odit distinctio autem!
  </p>
  <br />
  <h3>Descripción de la idea</h3>
  <p>
    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Eveniet blanditiis vel tenetur. Iste, nam est aspernatur repellendus earum inventore officiis nisi eligendi commodi iusto adipisci nobis a. Quam, suscipit sunt!
    Illum vitae doloremque, quam maiores quisquam corrupti eum, saepe sunt sit minima corporis nesciunt esse odit iure odio et deserunt voluptas! Delectus quae error distinctio asperiores neque nemo quo maiores.
    Voluptatibus laudantium, deleniti quasi officia harum quae neque temporibus libero aperiam quidem ducimus ipsum quis similique totam repellat exercitationem fugiat? Maxime, fugiat eligendi! Delectus voluptates minima facilis odit distinctio autem!
  </p>
  <br />
  <h3>Conclusión</h3>
  <p>
    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Eveniet blanditiis vel tenetur. Iste, nam est aspernatur repellendus earum inventore officiis nisi eligendi commodi iusto adipisci nobis a. Quam, suscipit sunt!
    Illum vitae doloremque, quam maiores quisquam corrupti eum, saepe sunt sit minima corporis nesciunt esse odit iure odio et deserunt voluptas! Delectus quae error distinctio asperiores neque nemo quo maiores.
    Voluptatibus laudantium, deleniti quasi officia harum quae neque temporibus libero aperiam quidem ducimus ipsum quis similique totam repellat exercitationem fugiat? Maxime, fugiat eligendi! Delectus voluptates minima facilis odit distinctio autem!
  </p>
`;

const documentElement = document.querySelector('.document');
documentElement.innerHTML = documentContent;
