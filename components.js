function renderCommentCard({ style, id }, container) {
  const card = document.createElement('div');
  card.classList.add('card');
  card.style = style;
  card.id = id;
  card.innerHTML = `
    <div class="card-userinfo">
      <div class="card-userinfo__avatar"></div>
      <div class="card-userinfo__text-container">
        <h4 class="card-userinfo__username">Cristian Acevedo</h4>
        <h5 class="card-userinfo__time">8:40 p.m Today</h5>
      </div>
    </div>
    <div class="card-comment">Este es un comentario</div>
  `;
  container.appendChild(card);
}
