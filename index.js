const documentEl = document.querySelector('.document');
const createBtn = document.getElementById('btn-create');
const commentsContainer = document.querySelector('.comments-container');

function highlight() {
  documentEl.contentEditable = true;
  document.execCommand('backColor', true, `#F2BC1B`); // highlight selection
  documentEl.contentEditable = false;
}

function undo() {
  documentEl.contentEditable = true;
  document.execCommand('undo');
  documentEl.contentEditable = false;
  window.getSelection().removeAllRanges();
}

function randomId() {
  return Math.random().toString(16).substring(2);
}

function detectInsertedNodes(e, id) {
  const insertedNode = e.target;
  if (insertedNode.nodeType === 1) {
    insertedNode.setAttribute('data-id', id);
  }
}

function renderComment(id) {
  const highlightText = document.querySelector(`span[data-id="${id}"]`);
  if (highlightText) {
    const { top } = highlightText.getBoundingClientRect();
    commentsContainer.setAttribute('style', 'width: 410px;');
    renderCommentCard({ style: `top: ${top - 10}px;`, id }, commentsContainer);
  }
}

function createComment() {
  const selection = window.getSelection();
  if (selection) {
    const id = randomId();
    const methodParams = [
      'DOMNodeInserted',
      (e) => detectInsertedNodes(e, id),
      false,
    ];

    documentEl.addEventListener(...methodParams);
    highlight();
    documentEl.removeEventListener(...methodParams);
    selection.removeAllRanges();
    renderButton(-50, 0);
    renderComment(id);
  }
}

function renderButton(position, scale = 1) {
  createBtn.setAttribute(
    'style',
    `top: ${position}px; transform: scale(${scale});`
  );
}

documentEl.addEventListener('mouseup', function () {
  const selection = window.getSelection();
  if (selection.type === 'Range') {
    const range = selection.getRangeAt(0);
    renderButton(range.getBoundingClientRect().top);
  } else {
    renderButton(-50, 0);
  }
});

createBtn.addEventListener('click', createComment);
